# Tranal #

Tranal is a package of Fortran programs for analysis of molecular
dynamics trajectories

### What is this repository for? ###

* Analysis of molecular dynamics trajectories

Supposrt trajactories from Gromacs (.xtc / .trr) , NAMD (.dcd ) , MDynaMix ,
in ASCII .xmol (.xyz) formats

* V. 6.0

### How do I get set up? ###

* Summary of set up

execute setup.sh
All executables can be found in "bin"

Manual: Currently as a part of MDynaMix  manual

Sample inputs: in directiory inputs

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Alexander Lyubartsev:  alexander.lyubartsev@mmk.su.se
