*  Main include file for tranal v.5.3 programs
      use tranalmod
      implicit real*8 (A-H,O-Z)
* max number of mol.types
      PARAMETER (NTPS=1024)
* max number pof trajectory files
      PARAMETER (MAXFIL=10000)
*
*  Math and Physical constants
*
      PARAMETER (AVSNO=6.02252D23,BOLTZ=1.38054D-23,FKCALJ=4.1868)
      PARAMETER (PI=3.1415926536D0,ELECHG=1.602D-19,EPS0=8.854D-12)
      PARAMETER (TOTPI=1.128379169D0,PI2=2.D0*PI)
      PARAMETER (TODGR=180.D0/PI,BOXYC=0.57735026918962579568)
*                                 
      real*4 DTN
      logical LXMOL,LVEL,LFILTR,LOCT,LHEX
      character*128  TAKESTR,STR,NAMOL,FILNAM,FILXM,FXMOL
      character*4  NFORM
      COMMON /INTEG/ ISTEP,IBREAK,NF,MEE,LF,LLNX,NTCF
      COMMON /FIXED/ NSPEC(NTPS),NSITS(NTPS),NAMOL(NTPS)
      COMMON/MISC/BREAKM,FULTIM1,VFAC,TEMP,EP,PRES,DTN
      COMMON /CONTRL/ NOP,NSITES,NTYPES,NSTOT,IAN,IPRINT,NUMTR,NS
      common /NAMES/ FILNAM(MAXFIL),TIMIN(MAXFIL)
     +  ,NFORM,FILXM,FXMOL
      COMMON /SIM/ BOXL,BOYL,BOZL,HBOXL,HBOYL,HBOZL,TOTMAS,FULTIM,
     + TSTART,TEND,VOL
      common/LOGLOG/LXMOL,LVEL,LFILTR,LHEX,LOCT

